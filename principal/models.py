from django.db import models

class PersonQuerySet(models.QuerySet):
	def boys(self):
		return self.filter(male=True)
	def girls(self):
		return self.filter(male=True)
	def less_than(self, age):
		return self.filter(age__lt = age) 

class Person(models.Model):
	name = models.CharField(max_length=30)
	male = models.BooleanField(default=True)
	age = models.IntegerField()
	objects = PersonQuerySet.as_manager()
	people = models.Manager()
